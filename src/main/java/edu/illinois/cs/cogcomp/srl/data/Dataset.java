package edu.illinois.cs.cogcomp.srl.data;

public enum Dataset {
	PTBAll, PTBTrain, PTBDev, PTBTest, PTB0204, PTBTrainDev
}
