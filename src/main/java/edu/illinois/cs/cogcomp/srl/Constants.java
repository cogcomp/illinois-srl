package edu.illinois.cs.cogcomp.srl;

/**
 * @author Vivek Srikumar
 * TODO Change this before shipping
 */
public class Constants {
	public final static String systemVersion = "5.4.1";

	public final static String systemName = "illinoisSRL";

	public final static String verbSRLSystemIdentifier = systemName + "-verb" + systemVersion;
	public final static String nomSRLSystemIdentifier = systemName + "-nom" + systemVersion;
}